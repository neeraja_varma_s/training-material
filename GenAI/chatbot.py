import streamlit as st
import time
from langchain.chains import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.chains import LLMChain
from langchain.llms import OpenAI
from langchain.memory import ConversationBufferMemory
from langchain.prompts import PromptTemplate
import openai
import os

# read local .env file
from dotenv import load_dotenv, find_dotenv

_ = load_dotenv(find_dotenv())  # read local .env file

openai.api_key = os.environ["OPENAI_API_KEY"]


st.title("Conversational GPT")
st.markdown(
    """
    This is conversational GPT
    """
)

if "memory" not in st.session_state:
    template = """You are a chatbot having a conversation with a human.

    {chat_history}
    Human: {human_input}
    Chatbot:"""

    prompt = PromptTemplate(
        input_variables=["chat_history", "human_input"], template=template
    )
    st.session_state.memory = ConversationBufferMemory(
        memory_key="chat_history")

    st.session_state.llm_chain = LLMChain(
        llm=ChatOpenAI(model_name="gpt-3.5-turbo", temperature=0),
        prompt=prompt,
        verbose=True,
        memory=st.session_state.memory,
    )

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []
    st.session_state.chat_history = []
    st.session_state.messages.append(
        {"role": "assistant", "content": "Hello there! How can I help you today ?"}
    )

if "qa_updated" not in st.session_state:
    st.session_state.qa_updated = False

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("Ask me a question"):
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    # Display assistant response in chat message container
    with st.chat_message("assistant"):
        message_placeholder = st.empty()
        full_response = ""

        # result = st.session_state.qa(
        #         {"question": prompt, "chat_history": st.session_state.chat_history}
        #     )
        # st.session_state.chat_history.append((prompt, result["answer"]))

        assistant_response = st.session_state.llm_chain.predict(
            human_input=prompt)

        # Simulate stream of response with milliseconds delay
        for chunk in assistant_response.split():
            full_response += chunk + " "
            time.sleep(0.05)
            # Add a blinking cursor to simulate typing
            message_placeholder.markdown(full_response + "▌")
        message_placeholder.markdown(full_response)
    # Add assistant response to chat history
    st.session_state.messages.append(
        {"role": "assistant", "content": full_response})
